# Requirements:
- Linux based OS
- Eclipse for C++ Developers
- cmake 3.10
- g++-6 if using cuda-9.0
- Cuda toolkit

# To initialize the project From this directory:
- chmod +x reintialize_project.sh 
- ./reintialize_project.sh


# If you want to add files to the project:
- add the files in the src directory
- add the file names to src/CMakeLists.txt
- run "./reintialize_project.sh" from this directory

# To open the project with eclipse:
- File Menu -> Import -> General -> Existing Project into workspace -> select the build/ directory 
