/*
 * MatrixMultiplication.cu
 *
 *  Created on: Jan 21, 2019
 *      Author: remon
 */

#include "MatrixMultiplication.h"

#include <iostream>
#include <chrono>
#include <ctime>


using namespace std;

void printMatrix(Matrix m){
	for(int i = 0; i < m.height; i++){
		for(int j = 0; j < m.width; j++){
			cout << m.elements[i * m.width + j] << ", ";
		}
		cout << endl;
	}
}
__host__ void createConstantMatrix(float constant, int width, int height, Matrix& out){
	//assert (width % BLOCK_SIZE) == 0; //we only work with multiples of block size
	//assert (height % BLOCK_SIZE) == 0; //we only work with multiples of block size
	out.width = width;
	out.height = height;
	out.elements = new float[width * height];
	for(int i = 0; i < (width * height); i++){
		out.elements[i] = constant;
	}
}

__host__ void MatMul(const Matrix A, const Matrix B, Matrix C){
	 // Load A and B to device memory
	    Matrix d_A;
	    d_A.width = A.width; d_A.height = A.height;
	    size_t size = A.width * A.height * sizeof(float);
	    cudaMalloc(&d_A.elements, size);
	    cudaMemcpy(d_A.elements, A.elements, size,
	               cudaMemcpyHostToDevice);
	    Matrix d_B;
	    d_B.width = B.width; d_B.height = B.height;
	    size = B.width * B.height * sizeof(float);
	    cudaMalloc(&d_B.elements, size);
	    cudaMemcpy(d_B.elements, B.elements, size,
	               cudaMemcpyHostToDevice);

	    // Allocate C in device memory
	    Matrix d_C;
	    d_C.width = C.width; d_C.height = C.height;
	    size = C.width * C.height * sizeof(float);
	    cudaMalloc(&d_C.elements, size);

	    // Invoke kernel
	    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	    dim3 dimGrid(B.width / dimBlock.x, A.height / dimBlock.y);
	    MatMulKernel<<<dimGrid, dimBlock>>>(d_A, d_B, d_C);

	    // Wait for GPU to finish before accessing on host
	    cudaDeviceSynchronize();

	    // Read C from device memory
	    cudaMemcpy(C.elements, d_C.elements, size,
	               cudaMemcpyDeviceToHost);

	    // Free device memory
	    cudaFree(d_A.elements);
	    cudaFree(d_B.elements);
	    cudaFree(d_C.elements);
}

// Matrix multiplication kernel called by MatMul()
__global__ void MatMulKernel(Matrix A, Matrix B, Matrix C)
{
    // Each thread computes one element of C
    // by accumulating results into Cvalue
    float Cvalue = 0;
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    for (int e = 0; e < A.width; ++e)
        Cvalue += A.elements[row * A.width + e]
                * B.elements[e * B.width + col];
    C.elements[row * C.width + col] = Cvalue;
}

void testMatrixMultiplication(){
	Matrix A, B, C, D;
	createConstantMatrix(5, 4*16*16, 4*16*16, A);
	createConstantMatrix(6, 4*16*16, 4*16*16, B);
	createConstantMatrix(-1, 4*16*16, 4*16*16, C);
	createConstantMatrix(4, 16, 16, D);

	//cout << "C: (before Multiplying)" << endl;
	//printMatrix(C);

	//cout << "A:" << endl;
	//printMatrix(A);
	//cout << "B:" << endl;
	//printMatrix(B);

	auto start = std::chrono::high_resolution_clock::now();
	CPUMatMul(A, B, C);
	auto end = std::chrono::high_resolution_clock::now();
	auto elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
	std::cout << "Multipilcation on cpu took " << elapsed_ms.count() << " micro-seconds" << std::endl;
	//printMatrix(C);

	start = std::chrono::high_resolution_clock::now();
	MatMul(A, B, C);
	end = std::chrono::high_resolution_clock::now();
	elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
	std::cout << "Multipilcation on gpu with global memory took " << elapsed_ms.count() << " micro-seconds" << std::endl;
	//cout << "C:" << endl;
	//printMatrix(C);

	//cout << "D:" << endl;
	//printMatrix(C);

	start = std::chrono::high_resolution_clock::now();
	SharedMatMul(A, B, C);
	end = std::chrono::high_resolution_clock::now();
	elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
	std::cout << "Multipilcation on gpu with shared memory took " << elapsed_ms.count() << " micro-seconds" << std::endl;
	//cout << "A * D:" << endl;
	//printMatrix(C);

}

// Get a matrix element
__device__ float GetElement(const Matrix A, int row, int col)
{
    return A.elements[row * A.stride + col];
}

// Set a matrix element
__device__ void SetElement(Matrix A, int row, int col,
                           float value)
{
    A.elements[row * A.stride + col] = value;
}

// Get the BLOCK_SIZExBLOCK_SIZE sub-matrix Asub of A that is
// located col sub-matrices to the right and row sub-matrices down
// from the upper-left corner of A
 __device__ Matrix GetSubMatrix(Matrix A, int row, int col)
{
    Matrix Asub;
    Asub.width    = BLOCK_SIZE;
    Asub.height   = BLOCK_SIZE;
    Asub.stride   = A.stride;
    Asub.elements = &A.elements[A.stride * BLOCK_SIZE * row
                                         + BLOCK_SIZE * col];
    return Asub;
}

 // Matrix multiplication - Host code
 // Matrix dimensions are assumed to be multiples of BLOCK_SIZE
 void SharedMatMul(const Matrix A, const Matrix B, Matrix C)
 {
     // Load A and B to device memory
     Matrix d_A;
     d_A.width = d_A.stride = A.width; d_A.height = A.height;
     size_t size = A.width * A.height * sizeof(float);
     cudaMalloc(&d_A.elements, size);
     cudaMemcpy(d_A.elements, A.elements, size,
                cudaMemcpyHostToDevice);
     Matrix d_B;
     d_B.width = d_B.stride = B.width; d_B.height = B.height;
     size = B.width * B.height * sizeof(float);
     cudaMalloc(&d_B.elements, size);
     cudaMemcpy(d_B.elements, B.elements, size,
     cudaMemcpyHostToDevice);

     // Allocate C in device memory
     Matrix d_C;
     d_C.width = d_C.stride = C.width; d_C.height = C.height;
     size = C.width * C.height * sizeof(float);
     cudaMalloc(&d_C.elements, size);

     // Invoke kernel
     dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
     dim3 dimGrid(B.width / dimBlock.x, A.height / dimBlock.y);
     SharedMatMulKernel<<<dimGrid, dimBlock>>>(d_A, d_B, d_C);

     // Wait for GPU to finish before accessing on host
     cudaDeviceSynchronize();

     // Read C from device memory
     cudaMemcpy(C.elements, d_C.elements, size,
                cudaMemcpyDeviceToHost);

     // Free device memory
     cudaFree(d_A.elements);
     cudaFree(d_B.elements);
     cudaFree(d_C.elements);
 }

 // Matrix multiplication kernel called by MatMul()
  __global__ void SharedMatMulKernel(Matrix A, Matrix B, Matrix C)
 {
     // Block row and column
     int blockRow = blockIdx.y;
     int blockCol = blockIdx.x;

     // Each thread block computes one sub-matrix Csub of C
     Matrix Csub = GetSubMatrix(C, blockRow, blockCol);

     // Each thread computes one element of Csub
     // by accumulating results into Cvalue
     float Cvalue = 0;

     // Thread row and column within Csub
     int row = threadIdx.y;
     int col = threadIdx.x;

     // Loop over all the sub-matrices of A and B that are
     // required to compute Csub
     // Multiply each pair of sub-matrices together
     // and accumulate the results
     for (int m = 0; m < (A.width / BLOCK_SIZE); ++m) {

         // Get sub-matrix Asub of A
         Matrix Asub = GetSubMatrix(A, blockRow, m);

         // Get sub-matrix Bsub of B
         Matrix Bsub = GetSubMatrix(B, m, blockCol);

         // Shared memory used to store Asub and Bsub respectively
         __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
         __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

         // Load Asub and Bsub from device memory to shared memory
         // Each thread loads one element of each sub-matrix
         As[row][col] = GetElement(Asub, row, col);
         Bs[row][col] = GetElement(Bsub, row, col);

         // Synchronize to make sure the sub-matrices are loaded
         // before starting the computation
         __syncthreads();
         // Multiply Asub and Bsub together
         for (int e = 0; e < BLOCK_SIZE; ++e)
             Cvalue += As[row][e] * Bs[e][col];

         // Synchronize to make sure that the preceding
         // computation is done before loading two new
         // sub-matrices of A and B in the next iteration
         __syncthreads();
     }

     // Write Csub to device memory
     // Each thread writes one element
     SetElement(Csub, row, col, Cvalue);
 }

  void CPUMatMul(const Matrix A, const Matrix B, Matrix& C){
  	C.height = A.height;
  	C.width = B.width;
  	//cout << C.height << ", " << C.width << ", " << A.width << endl;
  	C.elements = new float[C.height * C.width];
  	for(int i = 0; i < C.height; i++){
  		for(int j = 0; j < C.width; j++){
  			C.elements[i * C.width + j] = 0;
  			for(int k = 0; k < A.width; k++){
  				C.elements[i * C.width + j] += A.elements[i * A.width + k] * B.elements[k * B.width + j];
  			}
  			//cout << C.elements[i * C.width + j] << ", ";
  		}
  	}
  }
