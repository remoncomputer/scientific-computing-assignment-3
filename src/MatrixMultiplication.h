#include <cuda_runtime_api.h>
#include <cuda.h>


struct Matrix{
    int width;
    int height;
    int stride;
    float* elements;
};

// Thread block size
#define BLOCK_SIZE 16

// Forward declaration of the matrix multiplication kernel
__global__ void MatMulKernel(const Matrix, const Matrix, Matrix);
__global__ void SharedMatMulKernel(const Matrix, const Matrix, Matrix);
void MatMul(const Matrix A, const Matrix B, Matrix C);
Matrix createConstantMatrix(float constant, int width, int height);
void SharedMatMul(const Matrix A, const Matrix B, Matrix C);
void CPUMatMul(const Matrix A, const Matrix B, Matrix& C);
void testMatrixMultiplication();


