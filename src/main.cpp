#include <iostream>
#include <chrono>
#include <ctime>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "string.h"

#include <cuda_runtime_api.h>
#include <cuda.h>

#include "utils.h"
#include "prewittCPU.h"
#include "prewittGPU.h"
#include "MatrixMultiplication.h"

#define DEFAULT_THRESHOLD  4000

#define DEFAULT_FILENAME "../resources/BWstop-sign.ppm"

int main( int argc, char **argv )
{
	testMatrixMultiplication();

  int thresh = DEFAULT_THRESHOLD;
  char *filename;
  filename = strdup( DEFAULT_FILENAME);

  if (argc > 1) {
    if (argc == 3)  { // filename AND threshold
      filename = strdup( argv[1]);
       thresh = atoi( argv[2] );
    }
    if (argc == 2) { // default file but specified threshhold

      thresh = atoi( argv[1] );
    }

    fprintf(stderr, "file %s    threshold %d\n", filename, thresh);
  }


  int xsize, ysize, maxval;
  unsigned int * pic_unified_memory = read_ppm(filename, &xsize, &ysize, &maxval);

  unsigned int * result_unified_memory = NULL;
  int device = -1;
  cudaGetDevice(&device);
  cudaMallocManaged((void**)&result_unified_memory, xsize * ysize * sizeof(unsigned int));
  //Prefetches the data so timming don't count fetching the data.
  cudaMemPrefetchAsync(result_unified_memory, xsize * ysize * sizeof(unsigned int), device, NULL);
  auto start = std::chrono::high_resolution_clock::now();
  applyPrewittCPU(pic_unified_memory, xsize, ysize, thresh, result_unified_memory);
  auto end = std::chrono::high_resolution_clock::now();
  auto elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
  fprintf(stdout, "prewitt CPU done\n");
  std::cout << "It took " << elapsed_ms.count() << " micro-seconds" << std::endl;
  write_ppm(strdup("../resources/result_cpu.ppm"), xsize, ysize, maxval, result_unified_memory);

  start = std::chrono::high_resolution_clock::now();
  applyPrewittGlobalGPU(pic_unified_memory, xsize, ysize, thresh, result_unified_memory);
  end = std::chrono::high_resolution_clock::now();
  elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
  fprintf(stdout, "prewitt GPU global done \n");
  std::cout << "It took " << elapsed_ms.count() << " micro-seconds - This is for timing only review the other executable (yara.cu) for the actual implementation" << std::endl;
  write_ppm(strdup("../resources/result_gpu_global_test_only.ppm"), xsize, ysize, maxval, result_unified_memory);

  int block_height = 4*5;
  int block_width = 3*5;
  start = std::chrono::high_resolution_clock::now();
  applyPrewittSharedGPU(pic_unified_memory, xsize, ysize, thresh, result_unified_memory, block_height, block_width);
  end = std::chrono::high_resolution_clock::now();
  elapsed_ms = std::chrono::duration_cast<std::chrono::microseconds> (end - start);
  fprintf(stdout, "prewitt GPU shared done\n");
  std::cout << "It took " << elapsed_ms.count() << " micro-seconds" << std::endl;
  cudaMemPrefetchAsync(result_unified_memory, xsize * ysize * sizeof(unsigned int), device, NULL);
  write_ppm(strdup("../resources/result_gpu_shared.ppm"), xsize, ysize, maxval, result_unified_memory);

  cudaFree(result_unified_memory);
  cudaFree(pic_unified_memory);

}
