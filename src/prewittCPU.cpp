#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "string.h"



void applyPrewittCPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result){
	int numbytes =  xsize * ysize * 3 * sizeof( int );
	  /*unsigned int *result = (unsigned int *) malloc( numbytes );
	  if (!result) {
	    fprintf(stderr, "prewitt() unable to malloc %d bytes\n", numbytes);
	    exit(-1); // fail
	  }*/

	  int i, j, magnitude, sum1, sum2;

//	  for (int col=0; col<ysize; col++) {
//	    for (int row=0; row<xsize; row++) {
//	      *result++ = 0;
//	    }
//	  }

	  for (i = 0;  i < ysize; i++) {
	    for (j = 0; j < xsize; j++) {
	      int offset = i*xsize + j;
	      if(i == 0 || j == 0 || i == (ysize - 1) || j == (xsize - 1)){
	    	  result[offset] = 0;
	    	  continue;
	      }

	      sum1 =  pic[ xsize * (i-1) + j+1 ] -     pic[ xsize*(i-1) + j-1 ]
	        +     pic[ xsize * (i)   + j+1 ] -     pic[ xsize*(i)   + j-1 ]
	        +     pic[ xsize * (i+1) + j+1 ] -     pic[ xsize*(i+1) + j-1 ];

	      sum2 = pic[ xsize * (i-1) + j-1 ] +     pic[ xsize * (i-1) + j ]  + pic[ xsize * (i-1) + j+1 ]
	            - pic[xsize * (i+1) + j-1 ] -     pic[ xsize * (i+1) + j ] - pic[ xsize * (i+1) + j+1 ];

	      magnitude =  sum1*sum1 + sum2*sum2;

	      if (magnitude > thresh)
	        result[offset] = 255;
	      else
	        result[offset] = 0;
	    }
	  }
}
