#include <cuda_runtime_api.h>
#include <cuda.h>


__global__ void prewittGlobalMemoryGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result){
	int i = blockIdx.x;
	int j = threadIdx.x;
	if( i == 0 || j == 0 || i == (ysize - 1) || j == (xsize - 1)){
		result[i * xsize + j] = 0;
	}else if(i > (ysize - 1)  || j > (xsize - 1)){
		return;
	}else{
		int sum1 = pic[(i-1) * xsize + (j + 1)] - pic[(i-1)*xsize + (j - 1)]
				 + pic[(i) * xsize + (j + 1)]   - pic[(i)*xsize + (j - 1)]
				 + pic[(i+1) * xsize + (j + 1)] - pic[(i+1)*xsize + (j - 1)];
		
		int sum2 = pic[(i-1) * xsize + (j - 1)] + pic[(i-1) * xsize + (j)] + pic[(i-1) * xsize + (j + 1)]
				 - pic[(i+1) * xsize + (j - 1)] - pic[(i+1) * xsize + (j)] - pic[(i+1) * xsize + (j + 1)];
		int magnitude = sum1 * sum1 + sum2 * sum2;
		if(magnitude > thresh){
			result[i * xsize + j] = 255;
		}else {
			result[i * xsize + j] = 0;
		}
	}
}

void applyPrewittGlobalGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result){
	prewittGlobalMemoryGPU<<<ysize, xsize>>>(pic, xsize, ysize, thresh, result);
	
	// Wait for GPU to finish before accessing on host
  	cudaDeviceSynchronize();
}


__device__ bool calculateRelativeElementOffsetAndDetectIsItValid(int relativeElementIncrementInX,
										   int relativeElementIncrementInY,
										   int* relativeElementXOffsetInGlobalMemory,
										   int* relativeElementYOffsetInGlobalMemory,
										   int* relativeElementOffsetInGlobalMemory,
										   int* relativeElementOffsetInInputSharedMemory,
										   int currentElementXOffsetInGlobalMemory,
										   int currentElementYOffsetInGlobalMemory,
										   int currentElementOffsetInGlobalMemory,
										   int currentElementOffsetInInputSharedMemory,
										   int blockDimX,
										   int threadIdxX, int threadIdxY,
										   int xsize, int ysize){
	
	*relativeElementXOffsetInGlobalMemory = currentElementXOffsetInGlobalMemory + relativeElementIncrementInX;
	if(*relativeElementXOffsetInGlobalMemory < 0 || *relativeElementXOffsetInGlobalMemory >= xsize){
		return false;
	}
	
	*relativeElementYOffsetInGlobalMemory = currentElementYOffsetInGlobalMemory + relativeElementIncrementInY;
	if(*relativeElementYOffsetInGlobalMemory < 0 || *relativeElementYOffsetInGlobalMemory >= ysize){
		return false;
	}

	*relativeElementOffsetInGlobalMemory = (*relativeElementYOffsetInGlobalMemory) * xsize + (*relativeElementXOffsetInGlobalMemory);
	*relativeElementOffsetInInputSharedMemory = (threadIdxY + 1 + relativeElementIncrementInY) * (blockDimX + 2) + (threadIdxX + 1 + relativeElementIncrementInX);
	
	return true;
}

__device__ void copyDataFromBlockToInputSharedMemory(unsigned int * pic, int xsize, int ysize, 
									      unsigned char * inputBlock, int xOffsetInGlobalMemory,
									      int yOffsetInGlobalMemory, int offsetInGlobalMemory,
									      int offsetInInputSharedMemory, int blockDimX, int blockDimY,
									      int threadIdxX, int threadIdxY)
{
	
	int relativeElementXOffsetInGlobalMemory = -1;
	int relativeElementYOffsetInGlobalMemory = -1;
	int relativeElementOffsetInGlobalMemory = -1;
	int relativeElementOffsetInInputSharedMemory = -1;
	
	int relativeElementIncrementInX = 0; 
	int relativeElementIncrementInY = 0;
	
	//bool conditionDimMatrix[][] = {{true, false}, {false, true}, {true, true}};
	//bool conditionValues = {{0, blockDimX - 1}, {0, blockDimY - 1}};
	//int incrementMatrix[] = {-1, 1};
	bool isBorderPoint = (threadIdxX == 0) || (threadIdxX == blockDimX - 1) || (threadIdxY == 0) || (threadIdxY == blockDimY - 1);
										//{checkOnX, CheckOnY, XValue, YValue, XIncrement, YIncrement}
	int borderCheckAndProcedureMatrix[8][6] = {{1, 0, 0, -1, -1, 0},
										 {1, 0, blockDimX - 1, -1, 1, 0},
										 {0, 1, -1, 0, 0, -1},
										 {0, 1, -1, blockDimY - 1, 0, 1},
										 {1, 1,  0, 0, -1, -1},
										 {1, 1,  blockDimX - 1, 0, 1, -1},
										 {1, 1,  0, blockDimY - 1, -1, 1},
										 {1, 1,  blockDimX - 1, blockDimY - 1, 1, 1}};
	int* borderCondition;
	bool CheckOnX;
	bool CheckOnY;
	int targetXVal;
	int targetYVal;
	if(xOffsetInGlobalMemory < xsize && yOffsetInGlobalMemory < ysize)
	{
		inputBlock[offsetInInputSharedMemory] = pic[offsetInGlobalMemory];
		if(isBorderPoint == true)
		{
			for(int borderConditionIdx = 0; 
				borderConditionIdx < 8;
				borderConditionIdx++)
			{
					borderCondition = borderCheckAndProcedureMatrix[borderConditionIdx];
					CheckOnX = borderCondition[0] == 1;
					CheckOnY = borderCondition[1] == 1;
					targetXVal = borderCondition[2];
					targetYVal = borderCondition[3];
					relativeElementIncrementInX = borderCondition[4];
					relativeElementIncrementInY = borderCondition[5];
					if( (CheckOnX && threadIdxX == targetXVal) || (CheckOnY && threadIdxY == targetYVal))
					{
						if(calculateRelativeElementOffsetAndDetectIsItValid(relativeElementIncrementInX,
										   relativeElementIncrementInY,
										   &relativeElementXOffsetInGlobalMemory,
										   &relativeElementYOffsetInGlobalMemory,
										   &relativeElementOffsetInGlobalMemory,
										   &relativeElementOffsetInInputSharedMemory,
										   xOffsetInGlobalMemory,
										   yOffsetInGlobalMemory,
										   offsetInGlobalMemory,
										   offsetInInputSharedMemory,
										   blockDimX,
										   threadIdxX, threadIdxY,
										   xsize, ysize) == true)
						{
								inputBlock[relativeElementOffsetInInputSharedMemory] = pic[relativeElementOffsetInGlobalMemory];
						}

					}
			} 
		}
	}
}

/* //Unused Code
		bool condition[];
		bool checkOnX;
		bool checkOnY;
		int numberOfRepetitionOnXDim;
		int numberOfRepetitionOnYDim;
		for(int conditionIdx = 0; conditionIdx < sizeof(conditionDimMatrix); conditionIdx++){
			condition = conditionDimMatrix[conditionIdx];
			checkOnX = condition[0] && (threadIdxX == conditionValues[0][0]|| threadIdxX == conditionValues[0][1]);
			checkOnY = condition[1] && (threadIdxY == conditionValues[1][0]|| threadIdxY == conditionValues[1][1]); 
			numberOfRepetitionOnXDim = (checkOnX)? sizeof(incrementMatrix): 1;
			numberOfRepetitionOnYDim = (checkOnY)? sizeof(incrementMatrix): 1;
			for(int relativeElementIncrementInXIdx = 0; 
				relativeElementIncrementInXIdx < numberOfRepetitionOnXDim;
				relativeElementIncrementInXIdx++){
				relativeElementIncrementInX = (checkOnX)? incrementMatrix[relativeElementIncrementInXIdx]: 0;
				 
				for(int relativeElementIncrementInYIdx = 0; 
					relativeElementIncrementInYIdx < numberOfRepetitionOnYDim;
					relativeElementIncrementInYIdx++){
					relativeElementIncrementInY = (checkOnY)? incrementMatrix[relativeElementIncrementInYIdx]: 0;
					}
				} 
		}
		if(threadIdxX == 0){
			relativeElementIncrementInX = -1;
			relativeElementIncrementInY = 0;
			if(calculateRelativeElementOffsetAndDetectIsItValid(relativeElementIncrementInX, 
										   relativeElementIncrementInY,
										   &relativeElementXOffsetInGlobalMemory,
										   &relativeElementYOffsetInGlobalMemory,
										   &relativeElementOffsetInGlobalMemory,
										   &relativeElementOffsetInInputSharedMemory,
										   xOffsetInGlobalMemory,
										   yOffsetInGlobalMemory,
										   offsetInGlobalMemory,
										   offsetInInputSharedMemory,
										   blockDimX,
										   threadIdxX, threadIdxY,
										   xsize, ysize)){
				inputBlock[relativeElementOffsetInInputSharedMemory] = pic[relativeElementOffsetInGlobalMemory];
			}
		}else if(threadIdxX == blockDimX - 1){
			relativeElementIncrementInX = 1;
			relativeElementIncrementInY = 0;
			if(calculateRelativeElementOffsetAndDetectIsItValid(relativeElementIncrementInX, 
										   relativeElementIncrementInY,
										   &relativeElementXOffsetInGlobalMemory,
										   &relativeElementYOffsetInGlobalMemory,
										   &relativeElementOffsetInGlobalMemory,
										   &relativeElementOffsetInInputSharedMemory,
										   xOffsetInGlobalMemory,
										   yOffsetInGlobalMemory,
										   offsetInGlobalMemory,
										   offsetInInputSharedMemory,
										   blockDimX,
										   threadIdxX, threadIdxY,
										   xsize, ysize)){
				inputBlock[relativeElementOffsetInInputSharedMemory] = pic[relativeElementOffsetInGlobalMemory];
			}
		if(threadIdY == 0){
			relativeElementIncrementInX = 0;
			relativeElementIncrementInY = -1;
			if(calculateRelativeElementOffsetAndDetectIsItValid(relativeElementIncrementInX, 
										   relativeElementIncrementInY,
										   &relativeElementXOffsetInGlobalMemory,
										   &relativeElementYOffsetInGlobalMemory,
										   &relativeElementOffsetInGlobalMemory,
										   &relativeElementOffsetInInputSharedMemory,
										   xOffsetInGlobalMemory,
										   yOffsetInGlobalMemory,
										   offsetInGlobalMemory,
										   offsetInInputSharedMemory,
										   blockDimX,
										   threadIdxX, threadIdxY,
										   xsize, ysize)){
				inputBlock[relativeElementOffsetInInputSharedMemory] = pic[relativeElementOffsetInGlobalMemory];
			}
*/

__global__ void prewittSharedMemoryGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result){
	extern __shared__ unsigned char inputOutputBlock[];
	unsigned char * inputBlock = inputOutputBlock;
	//unsigned int * outputBlock = inputBlock;//inputOutputBlock + (blockDim.x + 2) * (blockDim.y + 2);
	int offsetInInputSharedMemory = (threadIdx.y + 1) * (blockDim.x + 2) + (threadIdx.x + 1);
	//int offsetInOutputSharedMemory = offsetInInputSharedMemory;//threadIdx.y * blockDim.x + threadIdx.x;
	int xOffsetInGlobalMemory = blockIdx.x * blockDim.x + threadIdx.x;
	int yOffsetInGlobalMemory = blockIdx.y * blockDim.y + threadIdx.y;
	int offsetInGlobalMemory = yOffsetInGlobalMemory * xsize + xOffsetInGlobalMemory;
	

	//Coping data from global memory to sharedMemory
	bool isInvalidPoint = yOffsetInGlobalMemory > (ysize - 1)  || xOffsetInGlobalMemory > (xsize - 1);
	if(isInvalidPoint){
		return;
	} 
	copyDataFromBlockToInputSharedMemory(pic, xsize, ysize, inputBlock, xOffsetInGlobalMemory,
									     yOffsetInGlobalMemory, offsetInGlobalMemory, offsetInInputSharedMemory, 
									     blockDim.x, blockDim.y, threadIdx.x, threadIdx.y);
	__syncthreads();
	
	unsigned char output;
	//Computing the Prewitt Filter
	if( yOffsetInGlobalMemory == 0 || xOffsetInGlobalMemory == 0 || 
		yOffsetInGlobalMemory == (ysize - 1) || xOffsetInGlobalMemory == (xsize - 1)){
		//outputBlock[offsetInOutputSharedMemory] = 0;
		output = 0;
	}else{
		int sum1 = inputBlock[(threadIdx.y + 1 - 1) * (blockDim.x + 2) + (threadIdx.x + 1 + 1)] - inputBlock[(threadIdx.y + 1 -1) * (blockDim.x + 2) + (threadIdx.x + 1 - 1)]
				 + inputBlock[(threadIdx.y + 1) * (blockDim.x + 2) + (threadIdx.x + 1 + 1)]   - inputBlock[(threadIdx.y + 1) * (blockDim.x + 2) + (threadIdx.x + 1 - 1)]
				 + inputBlock[(threadIdx.y + 1 + 1) * (blockDim.x + 2) + (threadIdx.x + 1 + 1)] - inputBlock[(threadIdx.y + 1 +1) * (blockDim.x + 2) + (threadIdx.x + 1 - 1)];
		
		int sum2 = inputBlock[(threadIdx.y + 1 - 1) * (blockDim.x + 2) + (threadIdx.x + 1 - 1)] + inputBlock[(threadIdx.y + 1 - 1) * (blockDim.x + 2) + (threadIdx.x + 1)] + inputBlock[(threadIdx.y + 1 - 1) * (blockDim.x + 2) + (threadIdx.x + 1 + 1)]
				 - inputBlock[(threadIdx.y + 1 + 1) * (blockDim.x + 2) + (threadIdx.x + 1 - 1)] - inputBlock[(threadIdx.y + 1 + 1) * (blockDim.x + 2) + (threadIdx.x + 1)] - inputBlock[(threadIdx.y + 1 + 1) * (blockDim.x + 2) + (threadIdx.x + 1 + 1)];
		
		int magnitude = sum1 * sum1 + sum2 * sum2;

		if(magnitude > thresh){
			//outputBlock[offsetInOutputSharedMemory] = 255;
			output = 255;
		}else {
			//outputBlock[offsetInOutputSharedMemory] = 0;
			output = 0;
		}
	}
	//__syncthreads();
	
	//Writting output to Global Memory
	result[offsetInGlobalMemory] = output;//outputBlock[offsetInOutputSharedMemory];

	//result[(blockIdx.y * blockDim.y + threadIdx.y)*xsize + (blockIdx.x * blockDim.x + threadIdx.x)] = 255;
}

void applyPrewittSharedGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result, int block_width, int block_height){

	int n_blocks_x = xsize / block_width + ((xsize % block_width > 0)? 1:0);
	int n_blocks_y = ysize / block_height + ((ysize % block_height > 0)? 1:0);
	
	dim3 dimBlock(n_blocks_x, n_blocks_y);  
	dim3 dimGrid(block_width, block_height);
	
	prewittSharedMemoryGPU<<<dimBlock, dimGrid, (block_width + 2) * (block_height + 2) * sizeof(unsigned char)>>>(pic, xsize, ysize, thresh, result);
 	
 	// Wait for GPU to finish before accessing on host
    cudaDeviceSynchronize();
}
