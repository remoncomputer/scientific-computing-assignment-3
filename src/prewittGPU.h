void applyPrewittGlobalGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result);
void applyPrewittSharedGPU(unsigned int * pic, int xsize, int ysize, int thresh, unsigned int * result, int block_width, int block_height);
