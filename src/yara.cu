
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "string.h"

#include <cuda_runtime_api.h>
#include <cuda.h>

#define DEFAULT_THRESHOLD  4000
#define DEFAULT_FILENAME "../resources/BWstop-sign.ppm"
#define BLOCK_SIZE 16


unsigned int *read_ppm( char *filename, int * xsize, int * ysize, int *maxval ){
  
  if ( !filename || filename[0] == '\0') {
    fprintf(stderr, "read_ppm but no file name\n");
    return NULL;  // fail
  }

  FILE *fp;

  fprintf(stderr, "read_ppm( %s )\n", filename);
  fp = fopen( filename, "rb");
  if (!fp) 
    {
      fprintf(stderr, "read_ppm()    ERROR  file '%s' cannot be opened for reading\n", filename);
      return NULL; // fail 

    }

  char chars[1024];
  //int num = read(fd, chars, 1000);
  int num = fread(chars, sizeof(char), 1000, fp);

  if (chars[0] != 'P' || chars[1] != '6') 
    {
      fprintf(stderr, "Texture::Texture()    ERROR  file '%s' does not start with \"P6\"  I am expecting a binary PPM file\n", filename);
      return NULL;
    }

  unsigned int width, height, maxvalue;


  char *ptr = chars+3; // P 6 newline
  if (*ptr == '#') // comment line! 
    {
      ptr = 1 + strstr(ptr, "\n");
    }

  num = sscanf(ptr, "%d\n%d\n%d",  &width, &height, &maxvalue);
  fprintf(stderr, "read %d things   width %d  height %d  maxval %d\n", num, width, height, maxvalue);  
  *xsize = width;
  *ysize = height;
  *maxval = maxvalue;
  
  unsigned int *pic = (unsigned int *)malloc( width * height * sizeof(unsigned int));
  if (!pic) {
    fprintf(stderr, "read_ppm()  unable to allocate %d x %d unsigned ints for the picture\n", width, height);
    return NULL; // fail but return
  }

  // allocate buffer to read the rest of the file into
  int bufsize =  3 * width * height * sizeof(unsigned char);
  if ((*maxval) > 255) bufsize *= 2;
  unsigned char *buf = (unsigned char *)malloc( bufsize );
  if (!buf) {
    fprintf(stderr, "read_ppm()  unable to allocate %d bytes of read buffer\n", bufsize);
    return NULL; // fail but return
  }





  // TODO really read
  char duh[80];
  char *line = chars;

  // find the start of the pixel data.   no doubt stupid
  sprintf(duh, "%d\0", *xsize);
  line = strstr(line, duh);
  //fprintf(stderr, "%s found at offset %d\n", duh, line-chars);
  line += strlen(duh) + 1;

  sprintf(duh, "%d\0", *ysize);
  line = strstr(line, duh);
  //fprintf(stderr, "%s found at offset %d\n", duh, line-chars);
  line += strlen(duh) + 1;

  sprintf(duh, "%d\0", *maxval);
  line = strstr(line, duh);


  fprintf(stderr, "%s found at offset %d\n", duh, line - chars);
  line += strlen(duh) + 1;

  long offset = line - chars;
  //lseek(fd, offset, SEEK_SET); // move to the correct offset
  fseek(fp, offset, SEEK_SET); // move to the correct offset
  //long numread = read(fd, buf, bufsize);
  long numread = fread(buf, sizeof(char), bufsize, fp);
  fprintf(stderr, "Texture %s   read %ld of %ld bytes\n", filename, numread, bufsize); 

  fclose(fp);


  int pixels = (*xsize) * (*ysize);
  for (int i=0; i<pixels; i++) pic[i] = (int) buf[3*i];  // red channel

 

  return pic; // success
}



void write_ppm( char *filename, int xsize, int ysize, int maxval, unsigned int *pic) 
{
  FILE *fp;
  int x,y;
  
  fp = fopen(filename, "w");
  if (!fp) 
    {
      fprintf(stderr, "FAILED TO OPEN FILE '%s' for writing\n");
      exit(-1); 
    }
  
  
  
  fprintf(fp, "P6\n"); 
  fprintf(fp,"%d %d\n%d\n", xsize, ysize, maxval);
  
  int numpix = xsize * ysize;
  for (int i=0; i<numpix; i++) {
    unsigned char uc = (unsigned char) pic[i];
    fprintf(fp, "%c%c%c", uc, uc, uc); 
  }
  fclose(fp);

}



typedef struct {
    int width;
    int height;
    unsigned int* elements;
} Matrix;


 
__global__ void prewittkernel(const Matrix, Matrix);

void prewitt(const Matrix img, Matrix output){
    
  Matrix d_img;
  Matrix d_output;

  d_img.width= img.width; d_img.height= img.height;
  size_t size =  img.width * img.height * sizeof(unsigned int);
  cudaMalloc(&d_img.elements, size);
  cudaMemcpy(d_img.elements, img.elements, size, cudaMemcpyHostToDevice);  
  

  d_output.width= output.width; d_output.height= output.height;
  size =  output.width * output.height * sizeof(unsigned int);
  cudaMalloc(&d_output.elements, size);
    
  
  int x_blocksPerGrid = ceil( (float)d_img.width/BLOCK_SIZE);

  int y_blocksPerGrid = ceil( (float)d_img.height/BLOCK_SIZE);
    
  dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
  
  dim3 dimGrid(x_blocksPerGrid, y_blocksPerGrid);  
    
  prewittkernel<<<dimGrid, dimBlock>>>(d_img, d_output);
  
  
   
    
  cudaMemcpy(output.elements, d_output.elements, size, cudaMemcpyDeviceToHost );
  cudaFree(d_img.elements);
  cudaFree(d_output.elements);

}


__global__
void prewittkernel(const Matrix img, Matrix output){
  //this version deals with unpadded images   
  int sum1,sum2,magnitude, THRESHOLD;
  int e_row, e_col;
  THRESHOLD = 4000 ;
    
  // In our implementation, each element of the output is calculated by one thread in a block
  // to determine each thread output location, we need to define the output element indices
  
    e_row= blockIdx.y * blockDim.y +threadIdx.y; // output row index
    e_col= blockIdx.x * blockDim.x + threadIdx.x; // output column index
   
    int step = img.width; 
    // threads on border will be idle
    if( (e_row > 0 && e_row < img.height-1) && (e_col > 0 && e_col < img.width-1)  ){
     
        sum1 = img.elements[(e_row -1 )*step + e_col+1] - img.elements[(e_row -1 )*step + e_col-1]
                + img.elements[ e_row*step + e_col+1] - img.elements[e_row*step + e_col-1 ]
                + img.elements[(e_row+1)*step + e_col+1] - img.elements[(e_row+1)*step + e_col -1];

        sum2 = img.elements[(e_row -1)*step + e_col+1] + img.elements[(e_row -1 )*step + e_col] + img.elements[(e_row -1 )*step + e_col+1]
                - img.elements[(e_row +1)*step + e_col-1] - img.elements[(e_row +1)*step + e_col] - img.elements[(e_row+1)*step + e_col+1];


        magnitude = sum1*sum1 + sum2*sum2;

        if (magnitude > THRESHOLD) {
          output.elements[(e_row)*step + e_col] = 255;} //Set to WHITE
        else{
          output.elements[(e_row)*step + e_col] = 0; } //Set to BLACK

      }
}
    
    


int main(void){

  Matrix pic, result;

    
  int thresh = DEFAULT_THRESHOLD;
  char *filename;
  filename = strdup(DEFAULT_FILENAME);
    
  int maxval;
  pic.elements = read_ppm( filename, &pic.width, &pic.height, &maxval ); 
  
  result.width= pic.width; result.height= pic.height;

  int numbytes =  pic.width * pic.height * sizeof( int );
  result.elements = (unsigned int *) malloc( numbytes );
  if (!result.elements) { 
    fprintf(stderr, "prewitt() unable to malloc %d bytes\n", numbytes);
    exit(-1); // fail
  }
    
  unsigned int *out = result.elements;

  for (int col=0; col<pic.width; col++) {
    for (int row=0; row<pic.height; row++) { 
      *out++ = 0; 
    }
  }
    


// function call
    prewitt( pic, result);

      
  write_ppm( "../resources/result_gpu_global_memory_official.ppm", pic.width, pic.height, 255, result.elements);

  fprintf(stderr, "prewitt done\n");
  return 0;
}
